# Resolución de sistema secuencial en verilog

## Cómo ejecutar el código

Utilizando [icarusverilog](http://iverilog.icarus.com/) versión 10.3
```console
iverilog modulos.v conexiones.v test_bench.v
```

Esto genera una salida "a.out", para ejecutarlo:

```console
./a.out
```

Los resultados no son satisfactorios

