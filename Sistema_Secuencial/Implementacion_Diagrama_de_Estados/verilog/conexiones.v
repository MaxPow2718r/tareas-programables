// Por simplificar el codigo
// A = COIN PRESENT
// B = CHANGER READY
// C = POP DROP READY
// D = SUMA MENOR 30
// E = SUMA IGUAL 30
// F = RELOJ
// G = CLEAR FFD
// w = DEC ACC
// x = CLR ACC
// y = RETURN NICKEL
// z = DROP POP

module conexiones(
		input A, B, C, D, E, F, G,
		output w, x, y, z
		);

		// Entradas de los FF
		wire t0, t1, t2;

		// Salidas de los FF
		wire q0, q1, q2;

		// Salidas negadas de los FF
		wire nq0, nq1, nq2;

		// ANDs para poder implemeter por SOP
		wire t01, t02, t03;
		wire t11, t12, t13, t14;
		wire t21, t22, t23, t24, t25;

		// Implemetacion de los FFD
		fft fft0(t0, F, G, q0, nq0);
		fft fft1(t1, F, G, q1, nq1);
		fft fft2(t2, F, G, q2, nq2);

		// Conexiones de salidas
		assign w = ((~A & B) & (~C & ~D)) & ((~E & q0) & (nq1 & nq2));
		assign x = ((~A & ~B) & (C & ~D)) & ((~E & q0) & (q1 & nq2));
		assign y = ((~A & ~B) & (~C & ~D)) & ((~E & q0) & (nq1 & nq2));
		assign z = ((~A & ~B) & (~C & ~D)) & ((~E & q0) & (q1 & nq2));

		// Conexiones de las entradas a los FFD
		// ANDs
		// d0
		assign t01 = ((~A & ~B) & (~C & ~D)) & ((nq0 & q1) & q2);
		assign t02 = ((~A & ~B) & (~C & ~D)) & ((~E & q0) & q2);

		// d1
		assign t11 = ((~A & ~B) & (~C & ~D)) & ((~E & nq0) & (nq1 & nq2));
		assign t12 = ((~A & ~B) & (~C & ~E)) & ((nq0 & q1) & q2);
		assign t13 = ((~A & ~B) & (~C & ~D)) & ((~E & q0) & q2);

		// d2
		assign t21 = ((A & ~B) & (~C & ~D)) & ((~E & nq0) & (nq1 & nq2));
		assign t22 = ((~A & ~B) & (~C & D)) & ((~E & nq0) & (q1 & q2));
		assign t23 = ((~A & ~B) & (~C & ~D)) & ((nq0) & (q1 & q2));
		assign t24 = ((~A & ~B) & (C & ~D)) & ((~E & q0) & (q1 & nq2));
		assign t25 = ((~A & ~B) & (~C & ~D)) & ((~E & q0) & (q1 & q2));
		assign t26 = ((~A & ~B) & (~C & ~D)) & ((~E & q0) & (nq1 & nq2));

		// ORs
		assign t0 = t01 | t02;
		assign t1 = (t11 | t12) | t13;
		assign t2 = ((t21 | t22) | (t23 | t24)) | (t25 | t26);
endmodule
