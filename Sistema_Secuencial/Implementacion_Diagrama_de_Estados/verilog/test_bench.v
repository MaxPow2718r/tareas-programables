module test;
	reg A, B, C, D, E, clk, rst;
	wire w, x, y, z;
	conexiones tb (.A(A), .B(B), .C(C), .D(D), .E(E), .F(clk),
					.G(rst), .w(w), .x(x), .y(y), .z(z));
	initial begin
			clk = 0;
			forever #1 clk = ~clk;
	end
	initial begin
			rst = 1; #100 ; rst = 0; #100;

			A = 0; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 1; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 1; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 1; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 0; E = 1; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 1; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 1; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			A = 0; B = 0; C = 0; D = 0; E = 0; #100;
			$write("A = %d; B = %d; C = %d; D = %d; E = %d | w = %d, x = %d, y = %d, z = %d\n",
					A, B, C, D, E, w, x, y, z);

			$finish;
	end
endmodule
