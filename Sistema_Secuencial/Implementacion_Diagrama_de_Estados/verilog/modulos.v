module fft(
		i, clk, clr, o, no
		);
		input i, clk, clr;
		output reg o, no;
		always@(posedge clk)
		begin
				if(!clr) begin
						o <= 0;
						no <= 1;
				end
				else begin
						if(i) begin
							o <= ~o;
							no <= o;
						end
						else begin
							o <= o;
							no <= ~o;
						end
				end
		end
endmodule
