# Resolución de sistema secuencial en vhdl

## Cómo ejecutar el código


Utilizando [GHDL](http://ghdl.free.fr/) versión 0.37

```console
ghdl -s conexiones.vhd conexiones_tb.vhd FFT.vhd
```

```console
ghdl -a conexiones.vhd conexiones_tb.vhd FFT.vhd
```


Elaboración y ejecución del código de pruebas
```console
ghdl -e conexiones_tb
```

```console
ghdl -r conexiones_tb
```

Los resultados no son satisfactorios

