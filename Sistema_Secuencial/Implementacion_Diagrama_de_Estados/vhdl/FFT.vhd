library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity FFT is
		PORT (
		T, RST, CLk	: in STD_LOGIC;
		Q, nQ		: out STD_LOGIC);
end FFT;

architecture behave of FFT is
begin
		process(T, CLK, RST)
		begin
				if (RST = '1') then
						Q 	<= '0';
						nQ 	<= '1';
				elsif(rising_edge(CLk)) then
						Q 	<= T;
						nQ	<= not T;

				end if;
		end process;
end behave;
