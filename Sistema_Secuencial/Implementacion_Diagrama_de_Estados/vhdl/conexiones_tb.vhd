library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity conexiones_tb is
end conexiones_tb;

architecture test of conexiones_tb is
		component conexiones
				port(
					A, B, C, D, E, F, G	: in std_logic;
					w, x, y, z			: out std_logic);
		end component;

		signal A, B, C, D, E, clk, rst	: std_logic;
		signal w, x, y, z				: std_logic;

begin
		tb : conexiones port map(A => A, B => B, C => C, D => D, E => E, F => clk, G => rst,
								w => w, x => x, y => y, z => z);
		process begin

			rst <= '1';
			wait for 100 ns;
			rst <= '0';
			wait for 100 ns;

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '1'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '1'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '1'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '1'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '1'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '1'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			A <= '0'; B <= '0'; C <= '0'; D <= '0'; E <= '0'; wait for 100 ns;
			report "A =" & std_logic'image(A) & "B =" & std_logic'image(B) & "C =" & std_logic'image(C) & "D =" & std_logic'image(D) & "E =" & std_logic'image(E)  & "|" & "w =" & std_logic'image(w) & "x =" & std_logic'image(x) & "y =" & std_logic'image(y) & "z =" & std_logic'image(z) & "\n";

			wait;

end process;
end test;

