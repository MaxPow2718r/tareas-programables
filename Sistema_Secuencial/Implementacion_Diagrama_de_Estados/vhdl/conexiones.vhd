-- Por simplificar el codigo
-- A = COIN PRESENT
-- B = CHANGER READY
-- C = POP DROP READY
-- D = SUMA MENOR 30
-- F = SUMA IGUAL 30
-- G = RELOJ
-- F = CLEAR FFD
-- w = DEC ACC
-- x = CLR ACC
-- y = RETURN NICKEL
-- z = DROP POP

library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity conexiones is
		port(
		A, B, C, D, E, F, G	: in STD_LOGIC;
		w, x, y, z			: out STD_LOGIC);
end entity;

architecture behave of conexiones is
		-- Entradas de los FF
		signal T0, T1, T2 : STD_LOGIC;

		-- Salidas de los FF
		signal Q0, Q1, Q2 : STD_LOGIC;

		-- Salidas negadas de los FF
		signal nQ0, nQ1, nQ2 : STD_LOGIC;

		-- ANDs para poder implemeter por SOP
		signal T01, T02 					: STD_LOGIC;
		signal T11, T12, T13 				: STD_LOGIC;
		signal T21, T22, T23, T24, T25, T26 : STD_LOGIC;

		component FFT is
				PORT (
				T, RST, CLk	: in STD_LOGIC;
				Q, nQ		: out STD_LOGIC);
		end component;

begin
		FFT0 : FFT
		port map(T	=> T0, RST	=> G, CLK	=> F, Q	=> Q0, nQ	=> nQ0);

		FFT1 : FFT
		port map(T	=> T1, RST	=> G, CLK	=> F, Q	=> Q1, nQ	=> nQ1);

		FFT2 : FFT
		port map(T	=> T2, RST	=> G, CLK	=> F, Q	=> Q2, nQ	=> nQ2);

		-- Conexiones de salidas
		w <= ((not A and B) and (not C and not D)) and ((not E and Q0) and (nQ1 and nQ2));
		x <= ((not A and not B) and (C and not D)) and ((not E and Q0) and (Q1 and nQ2));
		y <= ((not A and not B) and (not C and not D)) and ((not E and Q0) and (nQ1 and nQ2));
		z <= ((not A and not B) and (not C and not D)) and ((not E and Q0) and (Q1 and nQ2));

		-- Conexiones de las entradas a los FFD
		-- ANDs
		-- T0
		T01 <= ((not A and not B) and (not C and not D)) and ((nQ0 and Q1) and Q2);
		T02 <= ((not A and not B) and (not C and not D)) and ((not E and Q0) and Q2);

		-- T1
		T11 <= ((not A and not B) and (not C and not D)) and ((not E and nQ0) and (nQ1 and nQ2));
		T12 <= ((not A and not B) and (not C and not E)) and ((nQ0 and Q1) and Q2);
		T13 <= ((not A and not B) and (not C and not D)) and ((not E and Q0) and Q2);

		-- T2
		T21 <= ((A and not B) and (not C and not D)) and ((not E and nQ0) and (nQ1 and nQ2));
		T22 <= ((not A and not B) and (not C and D)) and ((not E and nQ0) and (Q1 and Q2));
		T23 <= ((not A and not B) and (not C and not D)) and ((nQ0) and (Q1 and Q2));
		T24 <= ((not A and not B) and (C and not D)) and ((not E and Q0) and (Q1 and nQ2));
		T25 <= ((not A and not B) and (not C and not D)) and ((not E and Q0) and (Q1 and Q2));
		T26 <= ((not A and not B) and (not C and not D)) and ((not E and Q0) and (nQ1 and nQ2));

		-- ORs
		T0 <= T01 or T02;
		T1 <= (T11 or T12) or T13;
		T2 <= ((T21 or T22) or (T23 or T24)) or (T25 or T26);

end behave;
