module maquina(
		input sysclk, clear, summ30, sum30, coinpresent, popdroprdy, changerrdy,
		input gnd, vcc,
		output clracc, nclracc, rtnnickel, decacc, droppop
		);

		// Principales
		wire sta, stb, stc, std, ste, stf, stg, d1, d2, d3, d4, A, B, C;
		// Auxiliares
		wire nA, nB, nC, nq1, q1, q2, q3, q4, odmux2;

		// MUX
		mux mux1(d2, gnd, gnd, gnd, summ30, vcc, gnd, vcc, gnd, A, B, C);
		mux mux2(d3, gnd, ~coinpresent, gnd, sum30, gnd, vcc, vcc, gnd, A, B, C);
		mux mux3(d4, coinpresent, vcc, gnd, gnd, changerrdy, vcc, popdroprdy, gnd, A, B, C);

		// FFD
		ffd ffd1(vcc, sysclk, clear, q1, nq1);
		ffd ffd2(d2, sysclk, clear, A, nA);
		ffd ffd3(d3, sysclk, clear, B, nB);
		ffd ffd4(d4, sysclk, clear, C, nC);

		ffd ffd5(~ste, sysclk, clear, clracc, nclracc);
		ffd ffd6(~stf, sysclk, clear, q2, rtnnickel);
		ffd ffd7(~stg, sysclk, clear, q3, decacc);
		ffd ffd8(~std, sysclk, clear, q4, droppop);

		// DEMUX
		demux demux1(vcc, sta, stb, odmux2, stc, stf, stg, std, ste, A, B, C);
endmodule
