module mux(
		z, i0, i1, i2, i3, i4, i5, i6, i7, s0, s1, s2
		);
		input wire i0, i1, i2, i3, i4, i5, i6, i7, s0, s1, s2;
		output reg z;
		always@(s0 & s1 & s2)
		begin
				case(s0 & s1 & s2)
						3'b000: z = i0;
						3'b001: z = i1;
						3'b010: z = i2;
						3'b011: z = i3;
						3'b100: z = i4;
						3'b101: z = i5;
						3'b110: z = i6;
						3'b111: z = i7;
						default : z = 1'b0;
				endcase
		end
endmodule

module demux(
		i, o0, o1, o2, o3, o4, o5, o6, o7, s0, s1, s2
		);
		input i, s0, s1, s2;
		output o0, o1, o2, o3, o4, o5, o6, o7;
		// Las salidas estan negadas
		assign	o0 = (i & ~s0 & ~s1 & ~s2),
				o1 = (i &  s0 & ~s1 & ~s2),
				o2 = (i & ~s0 &  s1 & ~s2),
				o3 = (i &  s0 &  s1 & ~s2),
				o4 = (i & ~s0 & ~s1 &  s2),
				o5 = (i &  s0 & ~s1 &  s2),
				o6 = (i & ~s0 &  s1 &  s2),
				o7 = (i &  s0 &  s1 &  s2);
endmodule

module ffd(
		i, clk, clr, o, no
		);
		input i, clk, clr;
		output reg o, no;
		always@(posedge clk)
		begin
				if(clr==1) begin
						o <= 0;
						no <= 1;
				end
				else if(clr==0) begin
						o <= i;
						no = !i;
				end
		end
endmodule
