module test;
		// inputs
		reg A, B, C, D, E, F, clk, rst;
		reg gnd, vcc;
		// outputs
		wire w, x, y, z;
		wire nx;
		maquina tb (.sysclk(clk), .clear(rst), .summ30(E),
				.sum30(F), .coinpresent(A), .popdroprdy(D),
				.changerrdy(B), .gnd(gnd), .vcc(vcc), .clracc(x),
				.nclracc(nx), .rtnnickel(y), .decacc(w), .droppop(z));
		initial begin

				A = 0; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 1; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 1; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 1; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 0; F = 1; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 1; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 1; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				A = 0; B = 0; D = 0; E = 0; F = 0; clk = 1; vcc = 1; gnd = 0; rst = 0; #10;
				$write("A = %d; B = %d; D = %d; E = %d; F = %d | w = %d; x = %d; y = %d; z = %d \n",
				A, B, D, E, F, w, x, y, z);

				$finish;
		end
endmodule
