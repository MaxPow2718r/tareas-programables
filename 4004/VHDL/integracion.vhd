-----------------------------------------------------------------
---ROM-4001------------------------------------------------------
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity ROM is
	port( io0_rom, io1_rom, io2_rom, io3_rom: in  std_logic;  ---solo entradas
    d0_rom, d1_rom, d2_rom, d3_rom: inout std_logic; ---buses de datos
    sync_rom, reset_rom, cm_rom, cl_rom : in std_logic; ---no todas esenciales
    clk1_rom, clk2_rom : in std_logic ---relojes externos
    );
end ROM;

architecture archrom of ROM is ---no hay reporte de la implementación
begin
d0_rom <= io0_rom;
d1_rom <= io1_rom;
d2_rom <= io2_rom;
d3_rom <= io3_rom;
end archrom;


------------------------------------------------------------------
---RAM-4002-------------------------------------------------------
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity RAM is
	port(o0_ram, o1_ram, o2_ram, o3_ram : out std_logic; --4 salidas ram
    d0_ram, d1_ram, d2_ram, d3_ram: inout std_logic; ---buses de datos
    sync_ram, reset_ram, cm_ram, po_ram : in std_logic;
	clk1_ram, clk2_ram : in std_logic
    );
end RAM;

architecture archram of RAM is -----no hay reporte de la implementación
begin
o0_ram <= d0_ram;
o1_ram <= d1_ram;
o2_ram <= d2_ram;
o3_ram <= d3_ram;
end archram;



--Instruction decorder y registro
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity instdecoder is
	port(instruccion : in std_logic_vector(3 downto 0);
    decodificado : out std_logic_vector(3 downto 0));
end instdecoder;

architecture archdeco of instdecoder is
begin
end instarchdeco;

--ALU
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity ALU is
	port(operando0, operando1 : in std_logic_vector(3 downto 0);
    resultado : out std_logic_vector(3 downto 0);
    instruccion : in std_logic);
end ALU;

architecture archalu of ALU is
begin
end archalu;

--Adress stack
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity adressstack is
	port(instruccion : in std_logic;
    mux : inout std_logic;
    muxvec: inout std_logic_vector (3 downto 0));
end adressstack;

architecture archadress of adressstack is
begin
end archadress;

--Timing and control
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity tcontrol is
	port(clk1, clk2 : in std_logic;
    test, reset : in std_logic;
    memorycontrol : out std_logic;
    arithcontrol: out std_logic;
    cm_rom, cm_ram0, cm_ram1, cm_ram2, cm_ram3 : out std_logic;
    sync : out std_logic);
end tcontrol;

architecture archcontrol of tcontrol is
begin
end archcontrol;

--Scratch pad
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity spad is
	port(control : in std_logic;
    mux : inout std_logic_vector (3 downto 0));
end spad;

architecture archpad of spad is
begin
end archpad;

------------------------------------------------------------------
---CPU-4004-------------------------------------------------------
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
entity CPU is
	port(d0_cpu, d1_cpu, d2_cpu, d3_cpu: inout std_logic; --buses de datos
    reset_cpu, test_cpu : in std_logic; --en espera
    sync_cpu, cm_ram_cpu, cm_rom_cpu : out std_logic;
    clk1_cpu, clk2_cpu : in std_logic
    );
end CPU;

architecture archcpu of CPU is

component instdecoder is
	port(instruccion : in std_logic_vector(3 downto 0);
    decodificado : out std_logic_vector(3 downto 0));
end component;

component ALU is
	port(operando0, operando1 : in std_logic_vector(3 downto 0);
    resultado : out std_logic_vector(3 downto 0);
    instruccion : in std_logic);
end component;

component adressstack is
	port(instruccion : in std_logic;
    mux : inout std_logic;
    muxvec: inout std_logic_vector (3 downto 0));
end component;

component tcontrol is
	port(clk1, clk2 : in std_logic;
    test, reset : in std_logic;
    memorycontrol : out std_logic;
    arithcontrol: out std_logic;
    cm_rom, cm_ram0, cm_ram1, cm_ram2, cm_ram3 : out std_logic;
    sync : out std_logic);
end component;

component spad is
	port(control : in std_logic;
    mux : inout std_logic_vector (3 downto 0));
end component;

begin
d0_cpu <= reset_cpu;
d1_cpu <= test_cpu;
d2_cpu <= clk1_cpu;
d3_cpu <= clk2_cpu;
end archcpu;


-- Deco de la RAM

library ieee;
use ieee.std_logic_1164.all;
--Decodifica las salidas (RAM) para el uso del semaforo(6 bits) .
-- Se utiliza 1 RAM (4 bits)

entity deco4x6 is

  port (
    ip0 : in  std_logic;   -- input
    ip1 : in  std_logic;   -- input
    ip2 : in  std_logic;   -- input
    ip3 : in  std_logic;   -- input

    NSVE : out std_logic;
    NSAM : out std_logic;
    NSRO : out std_logic;
    EWVE : out std_logic;
    EWAM : out std_logic;
    EWRO : out std_logic);  -- output

end deco4x6;

architecture beh of deco4x6 is
signal ip_CONCATENATE : std_logic_vector(3 downto 0);
begin  -- beh

deco : process (ip0,ip1,ip2,ip3)
  variable temp : std_logic_vector(5 downto 0);
  begin
  ip_CONCATENATE <= ip0 & ip1 & ip2 & ip3;
    case ip_CONCATENATE is
      when "0000" => temp := "100001";
      when "0001" => temp := "100010";
      when "0010" => temp := "001100";
      when "0011" => temp := "010100";
      when others => temp := "000000";
    end case;
    NSVE <= temp(5);
    NSAM <= temp(4);
    NSRO <= temp(3);
    EWVE <= temp(2);
    EWAM <= temp(1);
    EWRO <= temp(0);
 end process deco;
end beh;



------------------------------------------------------------------
---Component-integration------------------------------------------
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

entity integracion is
	port(
    ext_rst, test : in std_logic;
    clk1, clk2 : in std_logic;
    o0, o1, o2, o3, o4, o5 : out std_logic;
    i0, i1, i2, i3 : in std_logic
    );
end integracion;

architecture arch of integracion is
signal cm_ram_inter, cm_rom_inter : std_logic;
signal sinc_inter : std_logic;
signal d0, d1, d2, d3 : std_logic;
signal aux1, aux2 : std_logic;
signal oram0, oram1, oram2, oram3 : std_logic;

component ROM is
	port(
    io0_rom, io1_rom, io2_rom, io3_rom: in  std_logic;
    d0_rom, d1_rom, d2_rom, d3_rom: inout std_logic;
    sync_rom, reset_rom, cm_rom, cl_rom : in std_logic;
    clk1_rom, clk2_rom : in std_logic
    );
end component;

component RAM is
	port(
    o0_ram, o1_ram, o2_ram, o3_ram : out std_logic;
    d0_ram, d1_ram, d2_ram, d3_ram: inout std_logic;
    sync_ram, reset_ram, cm_ram, po_ram : in std_logic;
	clk1_ram, clk2_ram : in std_logic
    );
end component;

component CPU is
	port(
    d0_cpu, d1_cpu, d2_cpu, d3_cpu: inout std_logic;
    reset_cpu, test_cpu : in std_logic;
    sync_cpu, cm_ram_cpu, cm_rom_cpu : out std_logic;
    clk1_cpu, clk2_cpu : in std_logic
    );
end component;

component deco4x6 is
  port (
    ip0 : in  std_logic;   -- input
    ip1 : in  std_logic;   -- input
    ip2 : in  std_logic;   -- input
    ip3 : in  std_logic;   -- input
    NSVE : out std_logic;
    NSAM : out std_logic;
    NSRO : out std_logic;
    EWVE : out std_logic;
    EWAM : out std_logic;
    EWRO : out std_logic);  -- output
end component;

begin
cpu1 : CPU
port map (d0_cpu => d0,
	d1_cpu => d1,
	d2_cpu => d2,
	d3_cpu => d3,
	reset_cpu => ext_rst,
	test_cpu => test,
	sync_cpu => sinc_inter,
	cm_ram_cpu => cm_ram_inter,
	cm_rom_cpu => cm_rom_inter,
	clk1_cpu => clk1,
	clk2_cpu => clk2
);

rom1 : ROM
port map (io0_rom => i0,
	io1_rom => i1,
	io2_rom => i2,
	io3_rom => i3,
	d0_rom => d0,
	d1_rom => d1,
	d2_rom => d2,
	d3_rom => d3,
	sync_rom => sinc_inter,
	reset_rom => ext_rst,
	cm_rom => cm_rom_inter,
	cl_rom => aux1,
	clk1_rom => clk1,
	clk2_rom => clk2
);

ram1 : RAM
port map (o0_ram => oram0,
	o1_ram => oram1,
	o2_ram => oram2,
	o3_ram => oram3,
	d0_ram => d0,
	d1_ram => d1,
	d2_ram => d2,
	d3_ram => d3,
	sync_ram => sinc_inter,
	reset_ram => ext_rst,
	cm_ram => cm_ram_inter,
	po_ram => aux2,
	clk1_ram => clk1,
	clk2_ram => clk2
);

-- enlazar las salidas extras con las salidas de la ram
deco : deco4x6
port map (
    ip0 => oram0,
    ip1 => oram1,
    ip2 => oram2,
    ip3 => oram3,
    NSVE => o0,
    NSAM => o1,
    NSRO => o2,
    EWVE => o3,
    EWAM => o4,
    EWRO => o5
);

end arch;

