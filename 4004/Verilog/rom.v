//-ROM-4001------------------------------------------------------
module rom (
	io0_rom, io1_rom, io2_rom, io3_rom,
    d0_rom, d1_rom, d2_rom, d3_rom,
    sync_rom, reset_rom, cm_rom, cl_rom,
    clk1_rom, clk2_rom
    );

input sync_rom, reset_rom, cm_rom, cl_rom;
input clk1_rom, clk2_rom;
input io0_rom, io1_rom, io2_rom, io3_rom;
inout d0_rom, d1_rom, d2_rom, d3_rom;

// assign d0_rom = io0_rom;
// assign d1_rom = io1_rom;
// assign d2_rom = io2_rom;
// assign d3_rom = io3_rom;
endmodule

