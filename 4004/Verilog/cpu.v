//-CPU-4004-------------------------------------------------------
module cpu (
	d0_cpu, d1_cpu, d2_cpu, d3_cpu,
    reset_cpu, test_cpu,
    sync_cpu, cm_ram_cpu, cm_rom_cpu,
    clk1_cpu, clk2_cpu
    );

input reset_cpu, test_cpu;
input clk1_cpu, clk2_cpu;
output sync_cpu, cm_ram_cpu, cm_rom_cpu;
inout d0_cpu, d1_cpu, d2_cpu, d3_cpu;


wire [3:0] d0, d1, d2;
wire [3:0] memcontrol;
wire memcontrolalt;
wire clk1, clk2logic;
wire [3:0] tcontrolin;
wire arithcontrol;
wire memorycontrol;
wire test;
wire reset;
wire [3:0] decodificado;
wire control;
wire instruccion;
wire cm_rom;
wire sync;
wire [3:0] muxaddress;
wire cm_ram1, cm_ram2, cm_ram3;
wire resultado_alu;


instdecoder instdecoder1 (memcontrol, tcontrolin);

alu alu1 (d0, d1, resultado_alu, arithcontrol);

addressstack addressstack1 (memorycontrol, muxaddress, d2);

tcontrol tcontrol1 (clk1, clk2, test, reset, control, instruccion,
    cm_rom_cpu, cm_ram_cpu, cm_ram1, cm_ram2, cm_ram3, sync);

spad spad1 (memcontrolalt, d2);

assign d0_cpu = reset_cpu;
assign d1_cpu = test_cpu;
assign d2_cpu = clk1_cpu;
assign d3_cpu = clk2_cpu;
endmodule
