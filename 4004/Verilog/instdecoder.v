// Instruction decorder y registro
module instdecoder (
	instruccion, decodificado
	);

	input [3:0] instruccion;
	output [3:0] decodificado;

endmodule
