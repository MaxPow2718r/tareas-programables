// ALU
module alu (
	operando0, operando1, resultado, instruccion
	);

	input [3:0] operando0, operando1;
	input instruccion;
	output resultado;

endmodule
