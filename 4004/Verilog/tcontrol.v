//Timing and control
module tcontrol(
	clk1, clk2, test, reset, memorycontrol, arithcontrol,
    cm_rom, cm_ram0, cm_ram1, cm_ram2, cm_ram3,
    sync
	);

	input clk1, clk2;
	input test, reset;
	output memorycontrol;
	output arithcontrol;
	output cm_rom, cm_ram0, cm_ram1, cm_ram2, cm_ram3;
	output sync;


endmodule
