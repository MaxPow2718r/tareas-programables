// Decodifica las salidas (RAM) para el uso del semaforo(6 bits) .
// Se utiliza 1 RAM (4 bits)

module deco4x6 (ip0, ip1, ip2, ip3, nsve, nsam, nsro, ewve, ewam, ewro);

input ip0, ip1, ip2, ip3;
output reg nsve, nsam, nsro, ewve, ewam, ewro;  // output

wire [3:0] ip_CONCATENATE;
reg [5:0] temp;

assign ip_CONCATENATE = {ip0, ip1, ip2, ip3};

always@(ip0 & ip1 & ip2 & ip3)
begin
		case(ip0 & ip1 & ip2 & ip3)
				4'b0000 : temp = 6'b100001;
				4'b0001 : temp = 6'b100010;
				4'b0010 : temp = 6'b001100;
				4'b0011 : temp = 6'b010100;
				default : temp = 6'b000000;
		endcase

		nsve <= temp[5];
		nsam <= temp[4];
		nsro <= temp[3];
		ewve <= temp[2];
		ewam <= temp[1];
		ewro <= temp[0];
end
endmodule

