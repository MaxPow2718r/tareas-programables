//-RAM-4002-------------------------------------------------------
module ram
	(
	o0_ram, o1_ram, o2_ram, o3_ram,
    d0_ram, d1_ram, d2_ram, d3_ram,
    sync_ram, reset_ram, cm_ram, po_ram,
	clk1_ram, clk2_ram
    );

input sync_ram, reset_ram, cm_ram, po_ram;
input clk1_ram, clk2_ram;
output o0_ram, o1_ram, o2_ram, o3_ram;
inout d0_ram, d1_ram, d2_ram, d3_ram;

// assign o0_ram = d0_rami;
// assign o1_ram = d1_rami;
// assign o2_ram = d2_rami;
// assign o3_ram = d3_rami;
endmodule
