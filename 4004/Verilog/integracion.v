module integracion(
    input ext_rst, test, clk1, clk2, i0, i1, i2, i3,
    output o0, o1, o2, o3, o4, o5
    );

wire cm_ram_inter, cm_rom_inter;
wire sinc_inter;
wire aux1, aux2;
wire oram0, oram1, oram2, oram3;
wire d0, d1, d2, d3;

cpu cpu1 (d0, d1, d2, d3, ext_rst, test, sinc_inter, cm_ram_inter, cm_rom_inter, clk1, clk2);

rom rom1 (ii0, i1, i2, i3, d0, d1, d2, d3, sinc_inter, ext_rst, cm_rom_inter, aux1, clk1, clk2);

ram ram1 (oram0, oram1, oram2, oram3, d0, d1, d2, d3, sinc_inter, ext_rst, cm_ram_inter, aux2, clk1, clk2);

// enlazar las salidas extras con las salidas de la ram

deco4x6 deco (oram0, oram1, oram2, oram3, o0, o1, o2, o3, o4, o5);

endmodule
