library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity acertijo_tb is
end acertijo_tb;

architecture comportamiento of acertijo_tb is
component acertijo
end component;
begin
process begin
		for a in 0 to 9 loop
				for b in 0 to 9 loop
						if (10*b + a = 2*(10*a + b + 1)) then
  							report "La edad de mi tia es: " & integer'image(10*a + b);
  							wait;
						end if;
				end loop;
		end loop;
end process;
end comportamiento;
