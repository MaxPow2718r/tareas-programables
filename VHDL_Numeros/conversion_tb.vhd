library ieee;
use ieee.std_logic_1164.all;

entity testbench is
end testbench;

architecture test of testbench is
		component design
				port (
						a: in std_logic_vector(7 downto 0)
					 );
		end component;
		signal a : std_logic_vector(7 downto 0);
begin
		conv : design port map(a => a);
		process
        begin
				a <= x"10";
				assert false report "numeros" severity note;
                wait;
		end process;
end test;

