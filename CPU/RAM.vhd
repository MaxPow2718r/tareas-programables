------------------------------------------------------------------
---RAM-4002-------------------------------------------------------
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
entity RAM is
	port(o0, o1, o2, o3 : out std_logic;
    d0, d1, d2, d3: inout std_logic;
    sync, reset, cm_ram, po : in std_logic;
	clk1, clk2 : in std_logic
    );
end RAM;

architecture archram of RAM is
begin

end archram;

