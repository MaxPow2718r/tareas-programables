------------------------------------------------------------------
---CPU-4004-------------------------------------------------------
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
entity CPU is
	port(d0, d1, d2, d3: inout std_logic;
    reset, test : in std_logic;
    sync, cm_ram, cm_rom : out std_logic;
    clk1, clk2 : in std_logic;
    );
end CPU;

architecture archcpu of CPU is
begin

end archcpu;
