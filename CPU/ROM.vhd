------------------------------------------------------------------
---ROM-4001-------------------------------------------------------
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
entity ROM is
	port( io0, io1, io2, io3: inout  std_logic;
    d0, d1, d2, d3: inout std_logic;
    sync, reset, cm_rom, cl : in std_logic;
    clk1, clk2 : in std_logic
    );
end ROM;

architecture archrom of ROM is
begin

end archrom;
