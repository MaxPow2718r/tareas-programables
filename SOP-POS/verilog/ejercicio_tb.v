module test;
		wire out1, out2, out3, out4;
		reg W, X, Y, Z;
		formacanonica	tb1 (.A(X), .B(Y), .o1(out1), .o2(out2));
		formaminima		tb2 (.A(W), .B(X), .C(Y), .D(Z), .o1(out3), .o2(out4));

		initial begin
				$write("Problema de forma canonica\n");
				X = 0; Y = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d | SOP = %d, POS = %d", $time,X,Y,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");

				X = 0; Y = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d | SOP = %d, POS = %d", $time,X,Y,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");

				X = 1; Y = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d | SOP = %d, POS = %d", $time,X,Y,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");

				X = 1; Y = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d | SOP = %d, POS = %d", $time,X,Y,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");


				$write("Problema de minimizacion\n");
				W = 0; X = 0; Y = 0; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 0; X = 0; Y = 0; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 0; X = 0; Y = 1; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 0; X = 0; Y = 1; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 0; X = 1; Y = 0; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 0; X = 1; Y = 0; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 0; X = 1; Y = 1; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 0; X = 1; Y = 1; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 0; Y = 0; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 0; Y = 0; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 0; Y = 1; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 0; Y = 1; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 1; Y = 0; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 1; Y = 0; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 1; Y = 1; Z = 0; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");


				W = 1; X = 1; Y = 1; Z = 1; #10;
				$write("tiempo: %t | Entradas: A = %d, B = %d, C = %d, D = %d | SOP = %d, POS = %d", $time,W,X,Y,Z,out3,out4);
				if (out3 === out4)
						$write(" OK \n");
				else
						$write(" No OK \n");

		end
endmodule

