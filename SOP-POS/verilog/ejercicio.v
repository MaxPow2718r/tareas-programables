module formacanonica (input A,B, output o1,o2);
		// o1 es SOP
		assign o1 = ((~A) & (~B)) | ((~A) & B);
		// o2 es POS
		assign o2 = ((~A) | B) & ((~A) | (~B));
endmodule

module formaminima (input A,B,C,D, output o1, o2);
		// SOP
		assign o1 = (~B) & D;
		// POS
		assign o2 = ((~B)) & (D);
endmodule
