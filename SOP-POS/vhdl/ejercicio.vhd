-- Forma canonica
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity formacanonica is
		port(A		:in 	std_logic;
			 B		:in		std_logic;
			 o1		:out	std_logic;
			 o2		:out	std_logic);
end formacanonica;

architecture behave of formacanonica is
begin
		-- o1 es la forma SOP
		o1 <= ((not A) and (not B)) or ((not A) and B);
		-- o2 es la forma POS
		o2 <= ((not A) or B) and ((not A) or (not B));
end behave;

-- Forma minima
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity formaminima is
		port(A		:in 	std_logic;
			 B		:in		std_logic;
			 C		:in		std_logic;
			 D		:in		std_logic;
			 o1		:out	std_logic;
			 o2		:out	std_logic);
end formaminima;

architecture behave of formaminima is
begin
		-- o1 es la forma SOP
		o1 <= (not B) and (D);
		-- o2 es la forma POS
		o2 <= (not B) and (D);
end behave;
