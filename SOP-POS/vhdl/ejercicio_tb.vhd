library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ejercicio_tb is
end ejercicio_tb;


architecture test of ejercicio_tb is
		signal W, X, Y, Z, O1, O2, O3, O4 : std_logic;
		component formacanonica
				port(A		:in 	std_logic;
					 B		:in		std_logic;
					 o1		:out	std_logic;
					 o2		:out	std_logic);
		end component;
		component formaminima
				port(A		:in 	std_logic;
					 B		:in		std_logic;
					 C		:in		std_logic;
					 D		:in		std_logic;
					 o1		:out	std_logic;
					 o2		:out	std_logic);
		end component;
begin
		tb1 : formacanonica	port map(A => X, B => Y, o1 => O1, o2 => O2);
		tb2 : formaminima	port map(A => W, B => X, C => Y, D => Z, o1 => O3, o2 => O4);

		process begin

				report "Problema minimizacion";
				W <= '0';
				X <= '0';
				Y <= '0';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '0';
				X <= '0';
				Y <= '0';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '0';
				X <= '0';
				Y <= '1';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '0';
				X <= '0';
				Y <= '1';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '0';
				X <= '1';
				Y <= '0';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '0';
				X <= '1';
				Y <= '0';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '0';
				X <= '1';
				Y <= '1';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '0';
				X <= '1';
				Y <= '1';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '0';
				Y <= '0';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '0';
				Y <= '0';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '0';
				Y <= '1';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '0';
				Y <= '1';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '1';
				Y <= '0';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '1';
				Y <= '0';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '1';
				Y <= '1';
				Z <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				W <= '1';
				X <= '1';
				Y <= '1';
				Z <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(W) &
				", B = " & std_logic'image(X) &
				", C = " & std_logic'image(Y) &
				", D = " & std_logic'image(Z) &
				"Salidas: SOP = " &
				std_logic'image(O3) &
				", POS = " &
				std_logic'image(O4);
				if O3 = O4 then
						report " OK";
				else
						report " No OK";
				end if;

				report "Problema canonico";
				X <= '0';
				Y <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(X) &
				", B = " & std_logic'image(Y) &
				"Salidas: SOP = " &
				std_logic'image(O1) &
				", POS = " &
				std_logic'image(O2);
				if O1 = O2 then
						report " OK";
				else
						report " No OK";
				end if;

				X <= '0';
				Y <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(X) &
				", B = " & std_logic'image(Y) &
				"Salidas: SOP = " &
				std_logic'image(O1) &
				", POS = " &
				std_logic'image(O2);
				if O1 = O2 then
						report " OK";
				else
						report " No OK";
				end if;

				X <= '1';
				Y <= '0';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(X) &
				", B = " & std_logic'image(Y) &
				"Salidas: SOP = " &
				std_logic'image(O1) &
				", POS = " &
				std_logic'image(O2);
				if O1 = O2 then
						report " OK";
				else
						report " No OK";
				end if;

				X <= '1';
				Y <= '1';
				wait for 1 ns;
				report "Entradas: A = " & std_logic'image(X) &
				", B = " & std_logic'image(Y) &
				"Salidas: SOP = " &
				std_logic'image(O1) &
				", POS = " &
				std_logic'image(O2);
				if O1 = O2 then
						report " OK";
				else
						report " No OK";
				end if;

				wait;
		end process;
end test;
