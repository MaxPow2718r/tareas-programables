# Resolución de algebra de boole

## Como ejecutar los código

Utilizando [GHDL](http://ghdl.free.fr/) versión 0.37
Chequeo de sintaxis y análisis
```console
ghdl -s ejercicio* && ghdl -a ejercicio*
```

Elaboración y ejecución del código de pruebas
```console
ghdl -e ejercicio_tb && ghdl -r ejercicio_tb
```

