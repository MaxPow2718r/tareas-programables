# Tarea SOP-POS y minimización.

Para esta tarea en la minimización ambas funciones mínimas (por SOP y por POS) eran exactamente la misma: **(~B)(D)**. Por lo que a la hora de la implementación era evidente que los resultados iban a ser iguales. Sin embargo, la implementación completa se encuentra hecha como si estas dos fueran diferentes.
