library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity boole is
		port(x		:in 	std_logic;
			 y		:in		std_logic;
			 z		:in		std_logic;
			 o1		:out	std_logic;
			 o2		:out	std_logic);
end boole;

architecture behave of boole is
begin
		o1 <= ((x and y) or ((not(x)) and z)) or (y and z);
		o2 <= (x and y) or ((not(x)) and z);
end behave;
