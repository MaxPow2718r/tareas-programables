library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity boole_tb is
end boole_tb;


architecture test of boole_tb is
		component boole
				port(x		:in 	std_logic;
					 y		:in		std_logic;
					 z		:in		std_logic;
					 o1		:out	std_logic;
					 o2		:out	std_logic);
		end component;
		signal X, Y, Z, O1, O2 : std_logic;
begin
		tb : boole port map(x => X, y => Y, z => Z, o1 => O1, o2 => O2);
		process begin

				X <= '0';
				Y <= '0';
				Z <= '0';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '0';
				Y <= '0';
				Z <= '1';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '0';
				Y <= '1';
				Z <= '0';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '0';
				Y <= '1';
				Z <= '1';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '1';
				Y <= '0';
				Z <= '0';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '1';
				Y <= '0';
				Z <= '1';
				wait for 1 ns;
				if O1 = O2 then
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '1';
				Y <= '0';
				Z <= '1';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '1';
				Y <= '1';
				Z <= '0';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;

				X <= '1';
				Y <= '1';
				Z <= '1';
				wait for 1 ns;
				if O1 = O2 then
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" OK";
				else
						report "Entradas: x = " & std_logic'image(X) & ", y = " &
						std_logic'image(Y) & ", z = " &
						std_logic'image(Z) &
						"Salidas: Lado izquierdo = " &
						std_logic'image(O1) &
						", Lado derecho = " &
						std_logic'image(O2) &
						" No OK";
				end if;
				wait;
		end process;
end test;
