# Resolución de algebra de boole

## Como ejecutar los código

Utilizando [GHDL](http://ghdl.free.fr/) versión 0.37
Chequeo de sintaxis y análisis
```console
ghdl -s boole* && ghdl -a boole*
```

Elaboración y ejecución del código de pruebas
```console
ghdl -e boole_tb && ghdl -r boole_tb
```
