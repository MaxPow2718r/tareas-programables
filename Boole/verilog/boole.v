module boole (input x,y,z, output o1,o2);
		assign o1 = ((x & y) | ((~x) & z)) | (y & z);
		assign o2 = (x & y) | ((~x) & z);
endmodule
