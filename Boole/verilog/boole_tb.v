module test;
		wire out1, out2;
		reg X, Y, Z;
		boole tb (.x(X), .y(Y), .z(Z), .o1(out1), .o2(out2));

		initial begin
				X = 0; Y = 0; Z = 0; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
				X = 0; Y = 0; Z = 1; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
				X = 0; Y = 1; Z = 0; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
				X = 0; Y = 1; Z = 1; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
				X = 1; Y = 0; Z = 0; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
				X = 1; Y = 0; Z = 1; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
				X = 1; Y = 1; Z = 0; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
				X = 1; Y = 1; Z = 1; #10;
				$write("tiempo: %t | Entradas: X = %d, Y = %d, Z=%d | Lado izquierdo = %d, Lado derecho = %d", $time,X,Y,Z,out1,out2);
				if (out1 === out2)
						$write(" OK \n");
				else
						$write(" No OK \n");
		end
endmodule
