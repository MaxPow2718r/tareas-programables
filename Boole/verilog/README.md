# Resolicuón de algebra de boole en verilog

## Cómo ejecutar el código

Utilizando [icarusverilog](http://iverilog.icarus.com/) versión 10.3
```console
iverilog boole.v boole_tb.v
```

Esto genera una salida "a.out", para ejecutarlo:

```console
./a.out
```
